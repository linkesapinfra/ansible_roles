#/etc/profile.d/timeout.sh for SuSE Linux
#
# Timeout in seconds till the bash session is terminated
# in case of inactivity.
# 24h = 86400 sec
TMOUT=86400