# common
Playbook to setup EC2 instance for SAP software.

This role performs a set of actions in linux EC2 instances:
- Install all specifically required OS packages to SAP
- Set hostname
- Activate sapconf/saptune
- Install aws-cli
- Setup the GRand Unified Bootloader (GRUB)
- Set specific limits to OS
- Set ntp configuration (chrony)
- Activate system activity monitoring
- Enable all required services and disable all no-required services
- Mount EFS for stage
- Install aws agents (ssm, CloudWatch, AWS Data Provider for SAP)
- CIS hardening
- Create SAP users/groups
- Set Syntax ssh banner

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
- [Complete Common](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/common/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |

## Resources
| Name | Type |
|------|------|
| [common](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/common/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|efs_file_system_id|AWS EFS id|string|null|YES|
|efs_stage_dir|Filesystem for store staging software|string|/tmp/stage|YES|
|grub2config|grub file path|string|/etc/default/grub|YES|
|hidepid_option|allowed values: 0, 1, 2|number|2|no|
|modprobe_package|modprobe package|string|kmod-compat|YES|
|os_always_ignore_users|Users to exclude hardening|list(string)|['root', 'sync', 'shutdown', 'halt']|no|
|os_auth_gid_max|defaults for useradd|number|60000|no|
|os_auth_gid_min|defaults for useradd|number|1000|no|
|os_auth_lockout_time|OS user max lockout time for new login (seconds)|number|600|no|
|os_auth_pw_max_age|OS user max age|number|60|no|
|os_auth_pw_min_age|OS user min age (Discourage password cycling)|number|7|no|
|os_auth_pw_remember|How many used passwords are record|number|5|no|
|os_auth_retries|OS user max login retries|number|5|no|
|os_auth_sub_gid_count|defaults for useradd|number|65536|no|
|os_auth_sub_gid_max|defaults for useradd|number|600100000|no|
|os_auth_sub_gid_min|defaults for useradd|number|100000|no|
|os_auth_sub_uid_count|defaults for useradd|number|65536|no|
|os_auth_sub_uid_max|defaults for useradd|number|600100000|no|
|os_auth_sub_uid_min|defaults for useradd|number|100000|no|
|os_auth_sys_gid_max|defaults for useradd|number|999|no|
|os_auth_sys_gid_min|defaults for useradd|number|201|no|
|os_auth_sys_uid_max|defaults for useradd|number|999|no|
|os_auth_sys_uid_min|defaults for useradd|number|201|no|
|os_auth_timeout|Timeout to login|number|60|no|
|os_auth_uid_max|defaults for useradd|number|60000|no|
|os_auth_uid_min|defaults for useradd|number|1000|no|
|os_env_extra_user_paths|User paths to exclude hardening|list(string)|null|no|
|os_env_umask|Default umask|string|027|YES|
|os_ignore_users|User to exclude hardening|list(string)|['vagrant', 'kitchen', 'ec2-user']|no|
|os_shadow_perms/mode|shadow binary permissions|string|0640|YES|
|os_passwd_perms/mode|passwd binary permissions|string|0644|YES|
|os_useradd_mail_dir|defaults for useradd|string|/var/spool/mail|no|
|os_useradd_create_home|defaults for useradd|bool|true|no|
|selinuxconfig|config file for SElinux|string|/etc/selinux/config|no|
|sshdconfig|sshd_config file path|string|/etc/ssh/sshd_config|YES|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_cendure_token|CloudEndure Account Token|string|null|no|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
- Add support for non-SAP OS versions
- Add support for SuSe SLES 12
