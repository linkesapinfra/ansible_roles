# sap-ascs-install
Playbook to install SAP Server Central Services.

This role performs a set of actions in linux EC2 instances:
- Install SAP (A)SCS Instance
- Apply minimum custom parameters to DEFAULT profile
- **Only** available for _ABAP Stack_

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
- [Complete SAP Server Central Services](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-ascs-install/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) | >= 1.10 |

## Resources
| Name | Type |
|------|------|
| [appserver](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/appserver/) | role |
| [sap-ascs-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-ascs-install/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|aas_inst_num|SAP Instance number|string|01|YES|
|aas_virtual_hostname|Virtual hostname for SAP AAS|string|<hostname>|no|
|db_client_sar|Path for Oracle Client SAR file|string|null|YES|
|db_product|Database Product (SWPM Notation)|string|ORA|YES|
|kernel_dba_sar|Path for DBATL SAR file|string|null|YES|
|kernel_exe_sar|Path for SAPEXE SAR file|string|null|YES|
|kernel_exedb_sar|Path for SAPEXEDB SAR file|string|null|YES|
|kernel_helper_sar|Path for igshelper SAR file|string|null|YES|
|kernel_igs_sar|Path for igs SAR file|string|null|YES|
|kernel_jvm_sar|Path for SAPJVM SAR file|string|null|YES|
|sap_ascs_installdir|SAP Kernel packages path for SWPM|string|/tmp/stage/SAP_Kernel|YES|
|sap_swpm_sar_file_name|SWPM SAR file name|string|SWPM.SAR|YES|
|sap_swpm_sar_path|SWPM SAR file path|string|/tmp/stage/|YES|
|sapadm_uid|sapadm user id|string|1005|no|
|sapcar_file_name|SAPCAR binary file name|string|SAPCAR|YES|
|sapcar_path|SAPCAR binary file path|string|/tmp/stage/|YES|
|sapsid|SAP System ID|string|KKK|YES|
|sapsys_gid|sapsys group guid|string|1002|no|
|sidadm_uid|<sid>adm user id|string|900|YES|
|vault_abap_schema|ABAP schema|string|null|YES|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_java_schema|Java schema|string|null|no|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
- Add Java Stack
