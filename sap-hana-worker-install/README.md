# sap-hana-worker-install
Playbook to install SAP HANA Database worker/stand-by nodes on scale-out systems

This role performs a set of actions in linux EC2 instances:
- Set passwordless between nodes
- Install SAP HANA 2.0 components in worker node

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
No examples.

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| [sap hana](https://www.sap.com/products/hana.html) | >= 2.0 |
| [sap netweaver](https://www.sap.com/products/netweaver-platform.html) | >= 7.x |

## Resources
| Name | Type |
|------|------|
| [sap-hana-master-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-hana-master-install/) | role |
| [sap-hana-worker-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-hana-worker-install/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|sap_hana_coordinator_node_hostname|Hostname of SAP HANA Coordinator node|string|null|YES|
|sap_hana_coordinator_node_ip|IP address of SAP HANA Coordinator node|string|null|YES|
|sap_hana_coordinator_sidadm|<sid>adm account of SAP HANA Coordinator node|string|null|YES|
|sap_hana_deployment_bundle_path|SAP HANA SAR file path|string|/tmp/stage/|no|
|sap_hana_deployment_bundle_sar_file_name|SAP HANA SAR file name|string|null|YES|
|sap_hana_deployment_certificates_hostmap|Certificate Host Names|string|null|YES|
|sap_hana_deployment_components|SAP HANA components to install|list(string)|server,client|YES|
|sap_hana_deployment_create_initial_tenant|Create initial tenant database|string|y|no|
|sap_hana_deployment_datalog_volume_encryption|Encrypt data and log volumes at SAP HANA level|string|n|no|
|sap_hana_deployment_db_isolation|OS isolation level|string|low|YES|
|sap_hana_deployment_deploy_hostagent|Install SAP HOSTAGENT also. See [sap-hostagent-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-hostagent-install/) role instead.|string|n|YES|
|sap_hana_deployment_hana_env_type|SAP HANA environment type. Valid values: production, test, development or custom|string|production|YES|
|sap_hana_deployment_hana_groupid|ID of User Group (sapsys)|string|null|YES|
|sap_hana_deployment_hana_instance_number|SAP HANA Instance number|number|null|YES|
|sap_hana_deployment_hana_max_mem|Maximum Memory Allocation in MB|number|null|no|
|sap_hana_deployment_hana_mem_restrict|Set SAP HANA memory usage restriction|string|y|no|
|sap_hana_deployment_hana_sid|SAP HANA SID|string|null|YES|
|sap_hana_deployment_hana_userid|System Administrator User ID|string|null|YES|
|sap_hana_deployment_hostname|SAP HANA hostname|string|<hostname>|no|
|sap_hana_deployment_listen_interface|description|string|global|YES|
|sap_hana_deployment_lss_group|Local Secure Store User Group ID|string|null|no|
|sap_hana_deployment_lss_user|Local Secure Store User ID|string|null|no|
|sap_hana_deployment_sapcar_file_name|SAPCAR binary file name|string|SAPCAR|YES|
|sap_hana_deployment_sapcar_path|SAPCAR binary file path|string|/tmp/stage/|no|
|sap_hana_deployment_system_restart|Restart system after machine reboot?|string|n|no|
|sap_hana_deployment_use_master_password|Same password for all users accounts|string|n|no|
|sap_hana_deployment_xs_components|XS Advanced Components|list(string)|null|no|
|sap_hana_deployment_xs_components_nostart|Do not start the selected XS Advanced components after installation|string|none|no|
|sap_hana_deployment_xs_domain_name|XS Advanced Domain Name (see SAP Note 2245631)|string|null|no|
|sap_hana_deployment_xs_install|Install XS Advanced in the default tenant database?|string|n|no|
|sap_hana_deployment_xs_org_user|XS Advanced Admin User|string|XSA_ADMIN|no|
|sap_hana_deployment_xs_orgname|Organization Name For Space "SAP"|string|orgname|no|
|sap_hana_deployment_xs_path|XS Advanced App Working Path|string|null|no|
|sap_hana_deployment_xs_prod_space|Customer Space Name|string|PROD|no|
|sap_hana_deployment_xs_routing_mode|Routing Mode. Valid values: ports or hostnames|string|ports|no|
|sap_hana_deployment_xs_sap_space_user|XS Advanced SAP Space OS User ID|string|null|no|
|sap_hana_new_role|SAP HANA role to apply. Valid values: worker or standby|string|null|YES|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|
|vault_sap_hana_deployment_ase_user_password|SAP ASE Administrator Password|string|null|no|
|vault_sap_hana_deployment_common_master_password|Master Password|string|null|no|
|vault_sap_hana_deployment_hana_db_system_password|Database User (SYSTEM) Password|string|null|no|
|vault_sap_hana_deployment_lss_backup_password|Local Secure Store Auto Backup Password|string|null|no|
|vault_sap_hana_deployment_lss_user_password|Local Secure Store User Password|string|null|no|
|vault_sap_hana_deployment_sapadm_password|Password for sapadm user account|string|null|no|
|vault_sap_hana_deployment_sidadm_password|System Administrator Password|string|null|no|
|vault_sap_hana_deployment_root_password|Password for root user account|string|null|no|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
No roadmap.
