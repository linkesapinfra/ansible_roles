# sap-hostagent-install
Playbook to install SAP HostAgent.

This role performs a set of actions in linux EC2 instances:
- Install SAP HOSTAGENT
- Ensure SSL is configured for agent communication

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
- [Complete SAP HostAgent](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-hostagent-install/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| sshpass | >= 1.09 |

## Resources
| Name | Type |
|------|------|
| [common](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/common/) | role |
| [sap-hostagent-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-hostagent-install/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|sap_hostagent_agent_tmp_directory|Temporary directory for SAPHOSTAGENT files|string|/tmp/hostagent|YES|
|sap_hostagent_clean_tmp_directory|Remove the temporary directory after the installation has been done|bool|true|YES|
|sap_hostagent_config_ssl|Setup SSL for SAPHOSTAGENT|bool|true|YES|
|sap_hostagent_installation_type|SAPHOSTAGENT installation type|string|sar|YES|
|sap_hostagent_sar_file_name|ocal SAR file name|string|SAPHOSTAGENT.SAR|YES|
|sap_hostagent_sar_local_path|Local directory path where SAR file is located|string|null|YES|
|sap_hostagent_sapcar_file_name|Local SAPCAR tool file name|string|SAPCAR|YES|
|sap_hostagent_sapcar_local_path|Local directory path where SAPCAR tool file is located|string|null|YES|
|sap_hostagent_ssl_country|SSL Country Code parameter for SAPHOSTAGENT certificate|string|null|YES|
|sap_hostagent_ssl_org|SSL Organization parameter for SAPHOSTAGENT certificate|string|null|YES|
|sap_hostagent_ssl_passwd|Password for parameter for SAPHOSTAGENT certificate|string|null|YES|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
No roadmap.
