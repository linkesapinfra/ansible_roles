# sap-oracledb-install
Playbook to install Oracle Database 18c and 19c

This role performs a set of actions in linux EC2 instances:
- Install ORACLE DATABASE HOME
- Create SAP Users for Oracle Database
- Apply SAP Bundle Patch

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
- [Complete Oracle Database 19c](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-oracledb-install/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| [sap netweaver](https://www.sap.com/products/netweaver-platform.html) | >= 7.x |

## Resources
| Name | Type |
|------|------|
| [dbserver](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/dbserver/) | role |
| [sap-oracledb-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-oracledb-install/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|kernel_exe_sar|Path for SAPEXE SAR file|string|null|YES|
|kernel_helper_sar|Path for igshelper SAR file|string|null|YES|
|kernel_igs_sar|Path for igs SAR file|string|null|YES|
|kernel_jvm_sar|Path for SAPJVM SAR file|string|null|YES|
|sap_bundle_patch_file_name|SBP file name|string|SBP.ZIP|YES|
|sap_swpm_sar_file_name|SWPM SAR file name|string|SWPM.SAR|YES|
|sap_swpm_sar_path|SWPM SAR file path|string|/tmp/stage/|YES|
|sap_oradb_installdir|OracleDB packages path for SWPM|string|/tmp/stage/ORACLE|YES|
|sapadm_uid|sapadm user id|string|1005|no|
|sapcar_path|SAPCAR binary file path|string|/tmp/stage/|YES|
|sapcar_file_name|SAPCAR binary file name|string|SAPCAR|YES|
|sapinst_clientversion|Oracle Database Server version|number|19|YES|
|sapinst_dbsid|Database System ID|string|KKK|YES|
|sapinst_hasabap|sapadm user id|bool|true|no|
|sapinst_hasjava|sapadm user id|bool|false|no|
|sapinst_product|NetWeaver Product (SWPM Notation)|string|NW_Users_Create:GENERIC.ORA.PD|YES|
|sapinst_serverversion|Oracle Database Client version|number|19|YES|
|sapinst_sid|SAP System ID|string|KKK|YES|
|sapsys_gid|sapsys group guid|string|1002|no|
|sidadm_uid|<sid>adm user id|string|900|YES|
|runinstaller_path|RUNINSTALLER binary file path|string|/tmp/stage/ORACLE/LINUX_X86_64/db_home/SAP/|YES|
|vault_abap_schema|ABAP schema|string|SAPSR3|YES|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_java_schema|Java schema|string|SAPSR3DB|no|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|
|vault_root_pass|Password for root user account|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
No roadmap.
