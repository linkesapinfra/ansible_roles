# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
- Add support for non-SAP OS versions
- Add support for SAP ASE environments
- Add support for SAP WebDispatcher
- Add support for SAP BI/BO
- Add support for HA environments
- Add windows as OS supportted
- Set the execution status of the role at finish
- Check dependent roles at the start execution

## [0.0.1] - 2021-11-11
### Added
### Changed
* Roles documentation
### Removed
### Fixed
### Deprecated
* SAP JVM update
### Security


## [0.0.0] - 2021-11-04
### Added
* Initial release.
