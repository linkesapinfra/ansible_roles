# appserver
Playbook to setup EC2 instance for SAP AppServer.

This role performs a set of actions in linux EC2 instances:
- Set tune profile for SAP Netweaver Application Server
- Set filesystem layout for ALL SAP Netweaver (root, swap, /usr/sap, /usr/sap/DAA and /usr/sap/<SID>)
- Set specific limits to OS

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml

## Examples
- [Complete AppServer](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/appserver/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| sshpass | >= 1.09 |

## Resources
| Name | Type |
|------|------|
| [appserver](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/appserver/) | role |
| [common](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/common/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
No roadmap.
