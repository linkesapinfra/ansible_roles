# ansible_roles
[![semantic-release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
---
Ansible playbooks for install SAP products in AWS.

These roles carry out a set of actions in AWS EC2 instances in order to setup SAP environments:
- Common task for all EC2 instances
- Tasks for all SAP Application Servers
- Tasks for all SAP Database Servers
- Create filesystem layouts
- Install SAP HOSTAGENT
- Install SAP DAA
- Install SAP HANA Database 2.0 (Standalone and/or scale-out)
- Install SAP (A)SCS
- Install SAP PAS
- Install SAP AAS
- Install Oracle Database 18c & 19c
- System Copy (Parallel Export/Import) for Oracle
- Update SAP Kernel

## Usage
Check the role documentation.

## Run Locally
<details><summary><b>Show instructions</b></summary>
Clone the project

```bash
  git clone https://JavierSahagun@bitbucket.org/linkesapinfra/ansible_roles.git
```

Go to the project directory

```bash
  cd ansible_roles
```

Be fun!
</details>

## Resources
| Name | Type |
|------|------|
| [ansible_roles](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/) | resource |

## Authors
Playbooks are maintained by [Javier Sahagun](https://teams.microsoft.com/l/chat/0/0?users=javier.sahagun@syntax.com) and [Salvador Alavedra](https://teams.microsoft.com/l/chat/0/0?users=salvador.alavedra@syntax.com).

## FAQ
#### Why to Use This Solution?
You would use the related roles **only** if Syntax solution (Salt) isn't available for your customer.
#### This module helps enforce best practices?
AWS published [SAP Lens](https://docs.aws.amazon.com/wellarchitected/latest/sap-lens/sap-lens.pdf) and these ansible codes were created to help with some of the points listed there.
#### Rebuild from scratch is needed?
Existing AWS resources in your current AWS account(s) can be reused without downtime by these ansible roles via the ```ansible-playbook``` command. Be careful in your production environments, some settings can overwrite the existing ones.
#### Can I request new features?
See [Contributions](#Feedback)  section. Don't forget to check the current roadmap for each role and the [Change Log](CHANGELOG.md) before.

## Feedback
Contributions are always welcome! Please read the [contribution guidelines](CONTRIBUTING.md) first

## Reporting a Vulnerability
If you find a security vulnerability affecting any of the supported roles, please email to authors following the [guidelines](CONTRIBUTING.md).

After receiving the initial report, we will endeavor to keep you informed of the progress towards a fix and full announcement.
We may ask you for additional information. You are also welcome to propose a patch or solution.

Report security bugs in third-party software to the person or team maintaining it.

## License
[Apache 2 Licensed](http://www.apache.org/licenses/LICENSE-2.0) See LICENSE for full details.

## Appendix
Any additional information goes [here](https://www.syntax.com)
