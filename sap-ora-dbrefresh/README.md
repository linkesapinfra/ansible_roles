# sap-ora-dbrefresh
Playbook to refresh SAP DB instance based on Oracle DB with export/import method

This role performs a set of actions in linux EC2 instances:
- Execute Import tasks for SAP DB Refresh
- Re-install SAP PAS Instance
- **Only** available for _ABAP Stack_ over _Oracle 18c_ & _19c_

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
- [Complete refresh SAP DB instance](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-ora-dbrefresh/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| [sap netweaver](https://www.sap.com/products/netweaver-platform.html) | >= 7.x |

## Resources
| Name | Type |
|------|------|
| [appserver](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/appserver/) | role |
| [dbserver](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/dbserver/) | role |
| [sap-ora-dbrefresh](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-ora-dbrefresh/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|ascs_inst_num|SAP Instance number|string|00|YES|
|database_server_hostname|Database Server hostname|string|<hostname>|YES|
|db_client_sar|Path for Oracle Client SAR file|string|null|YES|
|db_product|Database Product (SWPM Notation)|string|ORA|YES|
|dbrefresh_export_filesystem|Value for Unicode is: '4103' on little endian (intel), '4102' on big endian. Value for Non-Unicode is: '1100' or country specific|string|4103|YES|
|dbrefresh_sapinst_chTablespace_checkbox|Check Tablespace Dialog|bool|false|YES|
|dbrefresh_sapinst_dbInstanceRam|Memory assigned to DB Instance|number|null|no|
|dbrefresh_sapinst_maxDatafileSize|Maximun Datafile Size|number|32000|YES|
|dbrefresh_sapinst_migmonComType|Communication type for the Migration Monitor. Possible values are: 'EXCHANGE', 'SOCKETS', none|string|EXCHANGE|YES|
|dbrefresh_sapinst_migmonJobNum|Migration Monitor Jobs. Recommended vCPU x 3|string|null|YES|
|dbrefresh_sapinst_migmonLoadArgs|Migration Monitor Load parameters|string|-loadprocedure fast -nolog -stop_on_error|YES|
|dbrefresh_sapinst_migrationKey|SAP MigrationKey|string|null|YES|
|dbrefresh_sapinst_tablespaceList|List of tablespaces to compress. Refer to SWPM manual|string|null|no|
|dbrefresh_sapinst_useParallelExportImport|Is parallel Export-Import procedure?. Possible values are: 'true' or empty value|bool|true|YES|
|dbsid|Database System ID|string|KKK|no|
|kernel_dba_sar|Path for DBATL SAR file|string|null|YES|
|kernel_exe_sar|Path for SAPEXE SAR file|string|null|YES|
|kernel_exedb_sar|Path for SAPEXEDB SAR file|string|null|YES|
|kernel_helper_sar|Path for igshelper SAR file|string|null|YES|
|kernel_igs_sar|Path for igs SAR file|string|null|YES|
|kernel_jvm_sar|Path for SAPJVM SAR file|string|null|YES|
|nw_product|NetWeaver Product (SWPM Notation)|string|BS2016.ERP608|YES|
|orasid|ora<sid> user|string|null|YES|
|pas_inst_num|SAP Instance number|string|01|YES|
|pas_virtual_hostname|Virtual hostname for SAP PAS|string|<hostname>|no|
|sap_swpm_sar_file_name|SWPM SAR file name|string|SWPM.SAR|YES|
|sap_swpm_sar_path|SWPM SAR file path|string|/tmp/stage/|YES|
|sap_pas_installdir|SAP Kernel packages path for SWPM|string|/tmp/stage/SAP_Kernel|YES|
|sapcar_file_name|SAPCAR binary file name|string|SAPCAR|YES|
|sapcar_path|SAPCAR binary file path|string|/tmp/stage/|YES|
|sapinst_pas_btc_wp_number|Number of BTC workprocess|number|6|no|
|sapinst_pas_clientVersion|Oracle Database Client version|number|19|YES|
|sapinst_pas_createGlobalProxyInfoFile|Create Global Proxy Info file|bool|false|no|
|sapinst_pas_createGlobalRegInfoFile|Create reginfo file|bool|true|no|
|sapinst_pas_dia_wp_number|Number of DIA workprocess|number|10|no|
|sapinst_pas_enableSPAMUpdateWithoutStackXml|Update SPAM transaction|bool|false|no|
|sapinst_pas_enableTMSConfigWithoutStackXml|Setup TMS (required by `sapinst_pas_enableSPAMUpdateWithoutStackXml`)|bool|false|no|
|sapinst_pas_enableTransportsWithoutStackXml|Setup TMS (required by `sapinst_pas_enableSPAMUpdateWithoutStackXml`)|bool|false|no|
|sapinst_pas_executeReportsForDepooling|Java schema|bool|true|YES|
|sapinst_pas_ms_configureAclInfo|SWPM ms_aclinfo|string|null|no|
|sapinst_pas_oralistenerPort|Oracle Listener Port|number|1527|YES|
|sapinst_pas_serverVersion|Oracle Database Server version|number|19|YES|
|sapsid|SAP System ID|string|KKK|YES|
|scs_virtual_hostname|Virtual hostname for SAP (A)ASCS|string|<hostname>|no|
|sidadm|<sid>adm user|string|null|YES|
|vault_abap_schema|ABAP schema|string|SAPSR3|YES|
|vault_abapSchemaPassword|Password for ABAP schema user account|string|null|YES|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_ddic_000_pass|Password for DDIC user in client 000|string|null|YES|
|vault_ddic_001_pass|Password for DDIC user in client 001|string|null|YES|
|vault_java_schema|Java schema|string|SAPSR3DB|no|
|vault_javaSchemaPassword|Password for Java schema user account|string|null|no|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|
|vault_ora_sys_password|Password for Oracle sys user account|string|null|YES|
|vault_ora_system_password|Password for Oracle system user account|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
No roadmap.
