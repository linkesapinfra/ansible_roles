# sap-kernel-update
Playbook to update SAP kernel with the downloaded SAR files

This role performs a set of actions in linux EC2 instances:
- Update SAP HOSTAGENT binary
- Update SAP EXE (DB Independent files)
- Update SAP EXEDB (DB dependent files)
- Update SAP Kernel database-libs files
- Update SAP IGS and igs helper
- Update SAP HANA CLIENT
- Update SAP JVM (method unsupported by SAP)

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
- [Complete SAP kernel update](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-kernel-update/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| [sap netweaver](https://www.sap.com/products/netweaver-platform.html) | >= 7.x |

## Resources
| Name | Type |
|------|------|
| [sap-kernel-update](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-kernel-update/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|ascs_inst_num|SAP (A)SCS Instance number|string|00|YES|
|sap_kernel_jvm_sar_file_name|Path for SAPJVM SAR file|string|null|YES|
|sap_kernel_sapcar_file_name|SAPCAR binary file name|string|SAPCAR|YES|
|sap_kernel_sapdbatools_sar_file_name|Path for DBATL SAR file|string|null|YES|
|sap_kernel_sapexe_sar_file_name|Path for SAPEXE SAR file|string|null|YES|
|sap_kernel_sapexedb_sar_file_name|Path for SAPEXEDB SAR file|string|null|YES|
|sap_kernel_saphanaclient_sar_file_name|Path for SAP HANA Client SAR file|string|null|YES|
|sap_kernel_saphostagent_sar_file_name|Path for SAPHOSTAGENT SAR file|string|null|YES|
|sap_kernel_sapigs_sar_file_name|Path for igs SAR file|string|null|YES|
|sap_kernel_sapigshelper_sar_file_name|Path for igshelper SAR file|string|null|YES|
|sap_kernel_sar_local_path|Local directory path where SAR files are located|string|null|YES|
|sap_kernel_tmp_directory|SAPCAR binary file path|string|/tmp/stage/|YES|
|sap_kernel_update_hanaclient|Execute update SAP HANA Client|bool|false|YES|
|sap_kernel_update_sapdbatools|Execute update DBATL|bool|true|YES|
|sap_kernel_update_sapexe|Execute update SAPEXE|bool|true|YES|
|sap_kernel_update_sapexedb|Execute update SAPEXEDB|bool|true|YES|
|sap_kernel_update_saphostagent|Execute update SAPHOSTAGENT|bool|true|YES|
|sap_kernel_update_sapigs|Execute update SAP igs and igshelper|bool|true|YES|
|sap_kernel_update_sapjvm|Execute update SAPJVM|bool|false|YES|
|sapsid|SAP System ID|string|null|YES|
|scs_virtual_hostname|Virtual hostname of SAP (A)SCS|string|null|no|
|sidadm|<sid>adm user|string|null|YES|
|pas_inst_num|SAP PAS Instance number|string|01|YES|
|pas_virtual_hostname|Virtual hostname of SAP PAS|string|null|no|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
No roadmap.
