# sap-daa-install
Playbook to install SAP Diagnostics Agent

This role performs a set of actions in linux EC2 instances:
- Install SAP DAA
- Apply minimum custom parameters to SAP HostControl Profile

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
- [Complete SAP Diagnostics Agent](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-daa-install/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html) | >= 1.10 |

## Resources
| Name | Type |
|------|------|
| [sap-daa-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-daa-install/) | role |
| [sap-hostagent-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-hostagent-install/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|daa_inst_num|SAP Instance number|string|97|YES|
|kernel_exe_sar|Path for SAPEXE SAR file|string|null|YES|
|kernel_helper_sar|Path for igshelper SAR file|string|null|YES|
|kernel_igs_sar|Path for igs SAR file|string|null|YES|
|kernel_jvm_sar|Path for SAPJVM SAR file|string|null|YES|
|kernel_smda_sar|Path for SMDA720 SAR file|string|null|YES|
|sap_daa_installdir|SAP Kernel packages path for SWPM|string|/tmp/stage/DAA|YES|
|sap_swpm_sar_file_name|SWPM SAR file name|string|SWPM.SAR|YES|
|sap_swpm_sar_path|SWPM SAR file path|string|/tmp/stage/|YES|
|sapadm_uid|sapadm user id|string|1005|no|
|sapcar_file_name|SAPCAR binary file name|string|SAPCAR|YES|
|sapcar_path|SAPCAR binary file path|string|/tmp/stage/|YES|
|sapsys_gid|sapsys group guid|string|1002|no|
|sidadm_uid|<sid>adm user id|string|900|YES|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
No roadmap.
