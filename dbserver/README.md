# dbserver
Playbook to setup EC2 instance for SAP Database Server.

This role performs a set of actions in linux EC2 instances:
- Install rpm's specific for ORACLEDB and HANADB
- Apply tune profile for database
- Allocate disks:
  - SAP HANA
  - SAP ASE
  - OracleDB
- Set filesystem layout

## Usage
`ansible-playbook [options] main.yml`

Check [Ansible documentation](https://docs.ansible.com/ansible/latest/cli/ansible-playbook.html) for all options available.

## Environment Variables
To run this role, you will need to add/check the variables to your .yml files:
- defaults/main.yml
- vars/main.yml
- vars/vault.yml

## Examples
- [Complete dbserver](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/dbserver/test/)

## Requirements
| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |
| sshpass | >= 1.09 |

## Resources
| Name | Type |
|------|------|
| [common](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/common/) | role |
| [dbserver](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/dbserver/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|vg_name_backup|VG name for Backup volume|string|VGbackup|YES|
|vg_name_bin|VG name for BIN volume|string|VGbin|YES|
|vg_name_data|VG name for Data volume|string|VGdata|YES|
|vg_name_log|VG name for Log/redo volume|string|VGlog|YES|
|vg_name_shared|VG name for Share/Home volume|string|VGshared|YES|
|vg_name_swap|VG name for SWAP volume|string|VGswap|YES|
|vault_aws_account_id|AWS Account ID|number|null|YES|
|vault_master_pass|Master Password for all sap user accounts|string|null|YES|

## Outputs
| Name | Description |
|------|-------------|

## Roadmap
No roadmap.
