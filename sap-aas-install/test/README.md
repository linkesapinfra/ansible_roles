# sap-aas-install example
Playbook in this directory configure linux EC2 instance for SAP Additional Application Server.

## Usage
To run this example you need to execute:
`ansible-playbook -i inventory test.yml`

Note that this example may create resources which cost money.

## Requirements

| Name | Version |
|------|---------|
| [ansible](https://docs.ansible.com/ansible/devel/reference_appendices/release_and_maintenance.html) | >= 2.10 |

## Resources

| Name | Type |
|------|------|
| [sap-aas-install](https://bitbucket.org/linkesapinfra/ansible_roles/src/master/sap-aas-install/) | role |

## Inputs
| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
|vars/main.yml|variables associated with this role|variables| |:YES:|
|vars/vault.yml|secured variables associated with this role|vault| |:YES:|
|defaults/main.yml|default lower priority variables for this role|variables| |:YES:|

## Outputs
No outputs.
